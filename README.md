# Space Observatory File I/O Activity

This activity simulates file input/output operations in the context of managing data for a space observatory. The exercise involves creating directories and files, writing to and reading from these files, and handling potential file-related errors.

## Objective

The primary objective of this activity is to familiarize participants with basic file and stream operations in C# including creating, writing to, reading from, and managing errors related to files and directories.

## Setup

1. Create directories for observational logs and data.
2. Generate initial files for logging observations and recording data.

## Tasks

1. **Log Daily Observations**: Write daily observations to a log file.
2. **Record Observational Data**: Populate a CSV file with fictional astronomical data.
3. **Read and Review Data**: Read the contents of the log and data files and display them.
4. **Error Handling**: Implement error handling for I/O operations.
5. **Bonus Challenge**: Process CSV data to filter specific observations.

## Implementation

- **Directories and Files Setup**: Initial setup for creating directories and files.
- **Write Operations**: Using `StreamWriter` to write logs and observational data.
- **Read Operations**: Using `StreamReader` to read and display file contents.
- **Error Handling**: Implementing try-catch blocks to manage I/O exceptions.
- **CSV Data Processing**: Utilizing a custom method to filter data from a CSV file.

## Conclusion

This activity offers hands-on experience with file I/O in C#, reinforcing concepts such as stream management, file manipulation, and error handling in a practical and engaging way.
