﻿namespace Noroff.Samples.PromFeatFileIOActivity
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Creating directories for logs and observational data
            Directory.CreateDirectory("ObservatoryData/TextLogs");
            Directory.CreateDirectory("ObservatoryData/ObservationalData");

            // Creating initial files and closing the FileStream
            File.Create("ObservatoryData/TextLogs/log.txt").Close();
            File.Create("ObservatoryData/ObservationalData/observations.csv").Close();

            // Writing sample log entries
            using (StreamWriter writer = new StreamWriter("ObservatoryData/TextLogs/log.txt"))
            {
                writer.WriteLine("2023-09-10: Noted unusual solar flare activity.");
                writer.WriteLine("2023-09-11: Meteor shower observed in the Orion belt.");
                writer.WriteLine("2023-09-12: Black hole event horizon visualized.");
            }

            // Populating the CSV file with fictional astronomical data
            using (StreamWriter writer = new StreamWriter("ObservatoryData/ObservationalData/observations.csv"))
            {
                writer.WriteLine("ID,Phenomenon,Magnitude,Timestamp");
                writer.WriteLine("1,Solar Flare,M6.5,2023-09-10T14:30:00");
                writer.WriteLine("2,Meteor Shower,M1.2,2023-09-11T21:15:00");
                writer.WriteLine("3,Black Hole Event,M9.8,2023-09-12T10:20:00");
            }


            // Reading and displaying the contents of the log file
            using (StreamReader reader = new StreamReader("ObservatoryData/TextLogs/log.txt"))
            {
                Console.WriteLine(reader.ReadToEnd());
            }

            // Reading and displaying the contents of the CSV file
            using (StreamReader reader = new StreamReader("ObservatoryData/ObservationalData/observations.csv"))
            {
                Console.WriteLine(reader.ReadToEnd());
            }

            // Wrapping file operations in try-catch for error handling
            try
            {
                // Code from previous steps goes here
            }
            catch (IOException ex)
            {
                Console.WriteLine($"Error accessing observatory data: {ex.Message}");
            }

            // BONUS: Using the FilterObservations method to find specific data
            foreach (var line in FilterObservations("ObservatoryData/ObservationalData/observations.csv", "Solar Flare"))
            {
                Console.WriteLine(line);
            }

        }

        // BONUS: Method to filter observations based on a specific criterion
        public static IEnumerable<string> FilterObservations(string path, string phenomenon)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (line.Contains(phenomenon))
                        yield return line;
                }
            }
        }
    }
}